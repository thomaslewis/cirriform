;;;; package.lisp

(defpackage #:cirriform
  (:use #:cl)
  (:export
   ;;;functions and methods
   #:password-login
   #:get-access-token
   #:joined-rooms
   #:send-room-message
   #:sync
   #:logout
   #:purge
   #:purge-status-query
   #:deactivate
   #:ban
   #:kick
   #:get-room-id
   #:remove-alias
   #:add-alias
   #:get-room-tags
   #:set-room-tag
   #:reset-password
   #:send-state-object
   #:get-room-state-object
   #:redact
   #:get-pushers
   #:pushers-set
   #:get-push-rules
   #:get-single-push-rule
   #:delete-push-rule
   #:add-push-rule
   #:push-rule-enabled?
   #:set-push-rule-enabled
   #:get-push-rule-actions
   #:change-push-rule-actions
   #:invite-to-room
   #:join-room
   #:leave-room
   #:forget-room
   #:unban-user
   #:upload-filter
   #:get-filter
   #:get-room-messages
   #:register-account
   #:media-post
   #:get-direct-chats
   #:create-room
   #:list-rooms

   ;;;classes
   #:matrix-session
   #:filter-event-filter
   #:filter-room-filter
   #:filter-room
   #:filter
   #:create-invite-3pid
   #:create-state-event
   #:create-room-args
   #:create-room-with-3pid-invite
   #:create-room-with-initial-state
   #:pusher-data
   #:pusher
   #:push-condition
   #:push-rule
   #:power-levels
   #:new-user
   #:room-state
   #:matrix-room

   ;;;accessors
   ;;matrix-session
   #:mxid
   #:token
   #:homeserver
   #:identity-server
   #:txn-id
   #:device-id
   #:display-name
   #:rooms
   #:next-batch

   ;;filter-event-filter
   #:limit
   #:not-senders
   #:not-types
   #:senders
   #:types

   ;;filter-room-filter
   #:not-rooms
   #:rooms
   #:contains-url

   ;;filter-room
   #:not-rooms
   #:rooms
   #:ephemeral
   #:include-leave
   #:state
   #:timeline
   #:account-data

   ;;filter
   #:event-fields
   #:event-format
   #:presence
   #:account-data
   #:room-slot

   ;;create-invite-3pid
   #:id-server
   #:medium
   #:address

   ;;create-state-event
   #:type-slot
   #:state-key
   #:content

   ;;create-room-args
   #:visibility
   #:room-alias
   #:room-name
   #:topic
   #:invite
   #:creation-content
   #:preset
   #:is-direct
   #:power-level

   ;;create-room-with-3pid-invite
   #:invite-3pid

   ;;create-room-with-initial-state
   #:initial-state

   ;;pusher-data
   #:url
   #:pusher-data-format

   ;;pusher
   #:pushkey
   #:kind
   #:app-id
   #:app-display-name
   #:device-display-name
   #:profile-tag
   #:lang
   #:data
   #:append-pusher

   ;;push-condition
   #:push-kind
   #:event-key
   #:event-pattern
   #:count-is

   ;;push-rule
   #:actions
   #:push-conditions
   #:content-pattern

   ;;power-levels
   #:ban-pl
   #:events-pl
   #:events-default-pl
   #:invite-pl
   #:kick-pl
   #:redact-pl
   #:state-default-pl
   #:users-pl
   #:users-default-pl
   #:notifications-pl

   ;;room-state
   #:join-rules
   #:power-levels
   #:room-name
   #:room-topic
   #:room-avatar
   #:pinned-events
   #:room-member-list

   ;;matrix-room
   #:room-id
   #:aliases
   #:room-state
   #:tags
   #:room-messages
   #:next-token

   ;;new-user
   #:auth
   #:bind-email
   #:username
   #:password
   #:dev-id
   #:init-dev-display-name
   #:inhibit-login))
