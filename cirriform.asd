;;;; cl-matrix.asd

(asdf:defsystem #:cirriform
  :description "A stateless Matrix client protocol library."
  :author "Thomas Lewis <thomaslewis1096@tutanota.com>"
  :license  "BSD 2-clause \"Simplified\" License"
  :version "0.0.1"
  :serial t
  :depends-on (#:dexador #:cl-json #:yason)
  :components ((:file "package")
               (:file "cirriform-classes" :depends-on ("package"))
               (:file "cirriform" :depends-on ("cirriform-classes"))))
