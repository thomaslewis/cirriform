;;;; cirriform.lisp

;;;(in-package #:cl-matrix)
(in-package :cirriform)

(defmethod matrix-put ((session matrix-session) endpoint &key (content-object nil) (want-stream nil))
  (setf (txn-id session) (1+ (txn-id session)))
  "Calls DEXADOR:PUT on the given Matrix endpoint, sending the data in CONTENT-OBJECT if supplied.
   Arguments:
   SESSION: an instance of MATRIX-SESSION for auth and homeserver information
   ENDPOINT: a Matrix endpoint to be appended to /_matrix/client/r0/
   CONTENT-OBJECT: an object capable of being encoded to JSON by JSON:ENCODE-JSON-TO-STRING. If not provided,
                   \"{}\" will be sent as the value of :CONTENT
   WANT-STREAM: T or NIL value which determines whether a hash table or stream is returned.
   Returns a hash table consisting of the key-value pairs in the JSON reply from the homeserver or the raw JSON stream
     depending on the value of WANT-STREAM."
  (let ((stream (dex:put (format nil "~A/_matrix/client/r0/~A" (homeserver session) endpoint)
                         :headers (if (and (slot-boundp session 'token) (token session))
                                      `(("Authorization" . ,(format nil "Bearer ~A" (token session))))
                                      '(("Content-Type" . "application/json")))
                         :content (if content-object
                                      (json:encode-json-to-string content-object)
                                      "{}")
                         :want-stream t)))
    (if want-stream stream (yason:parse stream))))

(defmethod matrix-post ((session matrix-session) endpoint &key (content-object nil) (want-stream nil))
  "Calls DEXADOR:PUT on the given Matrix endpoint, sending the data in CONTENT-OBJECT if supplied.
   Arguments:
   SESSION: an instance of MATRIX-SESSION for auth and homeserver information
   ENDPOINT: a Matrix endpoint to be appended to /_matrix/client/r0/
   CONTENT-OBJECT: an object capable of being encoded to JSON by JSON:ENCODE-JSON-TO-STRING. If not provided,
                   \"{}\" will be sent as the value of :CONTENT
   WANT-STREAM:T or NIL value which determines whether a stream or hash table will be returned
   Returns a hash table consisting of the key-value pairs in the JSON reply from the homeserver or the raw JSON
     stream depending on the value of WANT-STREAM."
  (setf (txn-id session) (1+ (txn-id session)))
  (let ((stream (dex:post (format nil "~A/_matrix/client/r0/~A" (homeserver session) endpoint)
                          :headers (if (and (slot-boundp session 'token) (token session))
                                       `(("Authorization" . ,(format nil "Bearer ~A" (token session)))
                                         ("Content-Type" . "application/json"))
                                       '(("Content-Type" . "application/json")))
                          :content (if (not (eql content-object nil))
                                       (json:encode-json-to-string content-object)
                                       "{}")
                          :want-stream t)))
    (if want-stream stream (yason:parse stream))))

(defmethod matrix-get ((session matrix-session) endpoint &key (want-stream nil))
  "Calls DEXADOR:GET on the given Matrix endpoint, returning a hash table containing the key-value pairs from the returned
   JSON object or the raw JSON stream depending on the value of WANT-STREAM.
   Arguments:
   SESSION: an instance of Matrix session for auth and homeserver data
   ENDPOINT: a Matrix endpoint to be appended to /_matrix/client/r0/
   WANT-STREAM: T or NIL value which determines whether a stream or hash table is returned."
  (let ((stream (dex:get (format nil "~A/_matrix/client/r0/~A" (homeserver session) endpoint)
                         :headers (if (and (slot-boundp session 'token) (token session))
                                      `(("Authorization" . ,(format nil "Bearer ~A" (token session))))
                                      ())
                         :want-stream t)))
    (if want-stream stream (yason:parse stream))))

(defmethod matrix-delete ((session matrix-session) endpoint &key (want-stream nil))
  "Calls DEXADOR:DELETE on the given Matrix endpoint, returning a hash table containing the key-value pairs from the
   returned JSON object or a stream containing the raw JSON response, depending on the value of WANT-STREAM.
   Arguments:
   SESSION: an instance of MATRIX-SESSION for auth and homeserver data
   ENDPOINT: a Matrix endpoint to be appended to /_matrix/client/r0/
   WANT-STREAM: T or NIL value which determines whether a stream or hash table will be returned"
  (let ((stream (dex:delete (format nil "~A/_matrix/client/r0/~A" (homeserver session) endpoint)
                            :headers (if (and (slot-boundp session 'token) (token session))
                                         `(("Authorization" . ,(format nil "Bearer ~A" (token session))))
                                         ())
                            :want-stream t)))
    (if want-stream stream (yason:parse stream))))

;;This function is only useful for single-level JSON objects. CLOS objects should be used to represent multi-level JSON objects.
(defun make-content-hash (key-list value-list)
  "Generates a hash table by consuming one key from key-list and one value from value-list in each iteration of the loop."
  (let ((content-hash (make-hash-table)))
    (do ((key (pop key-list) (pop key-list))
         (value (pop value-list) (pop value-list)))
        ((eql key nil) (setf (gethash key content-hash) value))
      (setf (gethash key content-hash) value))
    content-hash))

(defun password-login (session password)
  "PASSWORD-LOGIN sends the necessary information to a homeserver for a m.login.password login flow. Sets and returns
    (token session).
   Arguments:
   SESSION: an instance of MATRIX-SESSION for auth and homeserver data
   PASSWORD: password string for the MXID contained in SESSION"
  (let ((login (make-content-hash '("type" "user" "password") `("m.login.password" ,(mxid session) ,password))))
    (setf (token session) (gethash "access_token" (matrix-post session "login" :content-object login)))))

(defun joined-rooms (session)
  "Returns a list of all joined rooms for the MXID represented by ACCESS-TOKEN."
  (gethash "joined_rooms" (matrix-get session "joined_rooms")))

(defun send-room-message (session room-id msg-type message &key (want-stream nil))
  "Send a message to ROOM-ID using ACCESS-TOKEN for auth. Returns a hash table or stream containing the server reply.
   Arguments:
   SESSION: an instance of MATRIX-SESSION
   ROOM-ID: a Matrix RoomID to post message to
   MSG-TYPE: the Matrix message type
   MESSAGE: message content to be posted"
  (let ((content (make-content-hash '("body" "msgtype") `(,message ,msg-type))))
    (matrix-put session (format nil "rooms/~A/send/m.room.message/~A" room-id (txn-id session)) :content-object content :want-stream want-stream)))

(defun upload-filter (session filter)
  "Uploads a filter object to be used with calls to /sync
   SESSION: an instance of MATRIX-SESSION
   FILTER: an instance of FILTER"
  (matrix-post session (format nil "user/~A/filter" (mxid session)) :content-object filter))

(defun get-filter (session filter-id &key (want-stream nil))
  "Retrieves the filter object represented by FILTER-ID. Returns either a hash table or stream."
  (matrix-get session (format nil "user/~A/filter/~A" (mxid session) filter-id) :want-stream want-stream))

(defun sync (session &key (full-state-p nil) (since nil) (filter nil) (want-stream nil))
  "Performs a /sync request for the MXID represented by ACCESS-TOKEN. Returns a hash table or stream (with several layers
   of nesting if a hash table is requested)."
  (matrix-get session (format nil "sync?full_state=~A~A~A" (if full-state-p "true" "false")
                       (if since (format nil "&since=~A" since) "")
                       (if filter (format nil "&filter=~A" filter) "")) :want-stream want-stream))

(defun logout (session all-p &key (want-stream nil))
  "Sends a logout or logout all request to the homeserver. Reply is returned via hash table or stream. Sets the TOKEN slot of
   SESSION to nil in the supplied session object."
  (let ((reply (matrix-post session (format nil "logout~A" (if all-p "/all" "")) :want-stream want-stream)))
    (setf (token session) nil)
    reply))

(defun purge (session room-id
              &key (delete-local-events nil)
                (timestamp nil ts-supplied-p)
                (event-id nil event-id-supplied-p)
                (want-stream nil))
  "Submits a purge request to the homeserver for ROOM-ID. Returns hash table or stream containing the response for checking
   the status of the request."
  (matrix-post session (format nil "admin/purge_history/~A" room-id)
    :content-object (make-content-hash (cond (ts-supplied-p
                                              `("purge_up_to_ts" ,(if delete-local-events "delete_local_events")))
                                             (event-id-supplied-p
                                              `("purge_up_to_event_id" ,(if delete-local-events "delete_local_events")))
                                             (delete-local-events '("delete_local_events"))
                                             (t '()))
                                       (cond (ts-supplied-p
                                              `(,timestamp ,(if delete-local-events t)))
                                             (event-id-supplied-p
                                              `(,event-id ,(if delete-local-events t)))
                                             (delete-local-events '(t))
                                             (t '()))) :want-stream want-stream))

(defun purge-status-query (session purge-id &key (want-stream nil))
  "Checks status of a previously submitted purge request. Returns a hash table or stream containing the server reply."
  (matrix-get session (format nil "admin/purge_history_status/~A" purge-id) :want-stream want-stream))


(defun deactivate (session user-id &key (want-stream nil))
  "Deactivates a user account via Synapse's /admin API. Returns success or failure via a hash table or stream."
  (matrix-post session (format nil "admin/deactivate/~A" user-id) :want-stream want-stream))

(defun ban (session user-id room-id &optional (reason "") (want-stream nil))
  "Sends a ban event to a room. Returns a hash table or stream."
  (let ((content (make-content-hash '("user_id" "reason") `(,user-id ,reason))))
    (matrix-post session (format nil "rooms/~A/ban" room-id) :content-object content :want-stream want-stream)))

(defun kick (session user-id room-id &optional (reason "") (want-stream nil))
  "Issues a /kick request for USER-ID in ROOM-ID."
  (let ((content (make-content-hash '("user_id" "reason") `(,user-id ,reason))))
    (matrix-post session (format nil "rooms/~A/kick" room-id) :content-object content :want-stream want-stream)))

(defun get-room-id (session room-alias)
  "Retrieves a RoomID for an Alias and returns it as a string."
  (gethash "room_id" (matrix-get session (format nil "directory/room/%23~A" (string-left-trim "#" room-alias)))))

(defun remove-alias (session room-alias &key (want-stream nil))
  "Removes an alias from a room."
  (matrix-delete session (format nil "directory/room/%23~A" (string-left-trim "#" room-alias)) :want-stream want-stream))

(defun add-alias (session room-alias room-id &key (want-stream nil))
  "Adds an alias to ROOM-ID."
  (matrix-put session (format nil "directory/room/%23~A" (string-left-trim "#" room-alias))
    :content-object (make-content-hash '("room_id") `(,room-id)) :want-stream want-stream))

(defun get-room-tags (session user-id room-id &key (want-stream nil))
  "Retrieves the tags assigned to ROOM-ID for USER-ID."
  (matrix-get session (format nil "user/~A/rooms/~A/tags" user-id room-id) :want-stream want-stream))

(defun set-room-tag (session room-id tag order-between-0-1 &key (want-stream nil))
  "Sets room tags and priorities on ROOM-ID for USER-ID."
  (matrix-put session (format nil "user/~A/rooms/~A/tags/~A" (mxid session) room-id tag)
    :content-object (make-content-hash '("order") `(,order-between-0-1)) :want-stream want-stream))

(defun reset-password (session user-id new-password &key (want-stream nil))
  "Reset password for user indicated by USER-ID to NEW-PASSWORD."
  (matrix-post session (format nil "admin/reset_password/~A" user-id)
    :content-object (make-content-hash '("new_password") `(,new-password))
    :want-stream want-stream))

(defun get-room-state-object (session room-id state-event &key (want-stream nil))
  "Retrieves the requested room state object as a hash table or stream."
  (matrix-get session (format nil "rooms/~A/state/~A" room-id state-event) :want-stream want-stream))

(defun send-state-object (session room-id event-type content &key (want-stream nil))
  "Send a room state object to the server for ROOM-ID."
  (matrix-put session (format nil "rooms/~A/state/~A" room-id event-type) :content-object content :want-stream want-stream))

(defun redact (session room-id event-id reason &key (want-stream nil))
  "Redacts EVENT-ID in ROOM-ID."
  (matrix-put session (format nil "rooms/~A/redact/~A/~A" room-id event-id (txn-id session))
    :content-object (make-content-hash '("reason") `(,reason))
    :want-stream want-stream))

(defun get-pushers (session &key (want-stream nil))
  "Retrieves all pushers for the MXID contained in SESSION."
  (matrix-get session "pushers" :want-stream want-stream))

(defun pushers-set (session pusher &key (want-stream nil))
  "Sets a pusher for the MXID contained in SESSION."
  (matrix-post session "pushers/set" :content-object pusher :want-stream want-stream))

(defun get-push-rules (session &optional (scope "") (want-stream nil))
  "Retrieves push rules for MXID in SESSION, optionally using SCOPE to limit the scope of the returned push rules."
  (matrix-get session (format nil "pushrules~A" scope) :want-stream want-stream))

(defun get-single-push-rule (session scope kind rule-id &key (want-stream nil))
  "Retrieves the push rule named by RULE-ID."
  (matrix-get session (format nil "pushrules/~A/~A/~A" scope kind rule-id) :want-stream want-stream))

(defun delete-push-rule (session scope kind rule-id &key (want-stream nil))
  "Removes a push rule."
  (matrix-delete session (format nil "pushrules/~A/~A/~A" scope kind rule-id) :want-stream want-stream))

(defun add-push-rule (session scope kind rule-id push-rule &key (before nil) (after nil) (want-stream nil))
  "Uploads a new push rule."
  (matrix-put session (format nil "pushrules/~A/~A/~A~A" scope kind rule-id (cond (before (format nil "?before=~A" before))
                                                                                  (after (format nil "?after=~A" after))
                                                                                  (t (format nil ""))))
    :content-object push-rule :want-stream want-stream))

(defun push-rule-enabled? (session scope kind rule-id &key (want-stream nil))
  "Checks if a push rule is enabled or disabled."
  (matrix-get session (format nil "pushrules/~A/~A/~A/enabled" scope kind rule-id) :want-stream want-stream))

(defun set-push-rule-enabled (session scope kind rule-id enabled-p &key (want-stream nil))
  "Enables or disables a push rule."
  (matrix-put session (format nil "pushrules/~A/~A/~A/enabled" scope kind rule-id)
    :content-object (make-content-hash '("enabled") `(,enabled-p))
    :want-stream want-stream))

(defun get-push-rule-actions (session scope kind rule-id &key (want-stream nil))
  "Retrieves the actions associated with the given RULE-ID."
  (matrix-get session (format nil "pushrules/~A/~A/~A/actions" scope kind rule-id) :want-stream want-stream))

(defun change-push-rule-actions (session scope kind rule-id actions-list &key (want-stream nil))
  "Modifies push rule actions for RULE-ID."
  (matrix-put session (format nil "pushrules/~A/~A/~A/actions" scope kind rule-id)
    :content-object (make-content-hash '("actions") `(,actions-list))
    :want-stream want-stream))

(defun invite-to-room (session room-id invited-mxid &key (want-stream nil))
  "Invites INVITED-MXID to ROOM-ID."
  (matrix-post session (format nil "rooms/~A/invite" room-id)
    :content-object (make-content-hash '("user_id") `(,invited-mxid))
    :want-stream want-stream))

(defun join-room (session room-id &optional 3pid-invite-signed (want-stream nil))
  "Joins ROOM-ID for the MXID contained in SESSION."
  (matrix-post session (format nil "rooms/~A/join" room-id)
    :content-object (if 3pid-invite-signed 3pid-invite-signed nil)
    :want-stream want-stream))

(defun leave-room (session room-id &key (want-stream nil))
  "Leaves a room."
  (matrix-post session (format nil "rooms/~A/leave" room-id) :want-stream want-stream))

(defun forget-room (session room-id &key (want-stream nil))
  "Causes the MXID in SESSION to forget ROOM-ID."
  (matrix-post session (format nil "rooms/~A/forget" room-id) :want-stream want-stream))

(defun unban-user (session room-id banned-mxid &key (want-stream nil))
  "Unbans BANNED-MXID from ROOM-ID."
  (matrix-post session (format nil "rooms/~A/unban" room-id)
    :content-object (make-content-hash '("user_id") `(,banned-mxid))
    :want-stream want-stream))

(defun get-room-messages (session room-id &key from (to nil) (dir "f") limit (want-stream nil))
  "Retrieves messages hash table for ROOM-ID."
  (matrix-get session (format nil "rooms/~A/messages?~A" room-id
                       (concatenate 'string
                                    "from=" from
                                    (if to (progn "&to=" to))
                                    "&dir=" dir
                                    (if limit (progn "&limit=" limit))))
    :want-stream want-stream))

(defun register-account (session new-user)
  "Creates a new account on the homeserver designated in SESSION using the data in the object NEW-USER.
   MXID of SESSION will be set to the new MXID. Sets and returns the access token for SESSION."
  (setf (auth new-user) `(("type" . "m.login.dummy")
                          ,(cons "session"
                                 (handler-case
                                     (dex:post (format nil "~A/_matrix/client/r0/register"
                                                       (homeserver session))
                                               :content "{}"
                                               :want-stream t)
                                   (dexador.error:http-request-unauthorized (request)
                                     (gethash "session"
                                              (yason:parse (dexador.error:response-body request))))))))
  (let ((response-hash (matrix-post session "register" new-user)))
    (if (gethash "access_token" response-hash)
        (progn
          (setf (mxid session) (gethash "user_id" response-hash))
          (setf (device-id session) (gethash "device_id" response-hash))
          (setf (token session) (gethash "access_token" response-hash))))))

(defmethod media-post ((session matrix-session) file)
  (setf (txn-id session) (1+ (txn-id session)))
  "Calls DEXADOR:PUT to upload FILE to HOMESERVER in SESSION and returns the URI for the uploaded media.
   Arguments:
   SESSION: an instance of MATRIX-SESSION for auth and homeserver information
   FILE: a pathname pointing to the file to upload
   Returns the content URI from the homeserver response as a string."
  (gethash "content_uri"
           (yason:parse (dex:post (format nil "~A/_matrix/media/r0/upload?filename=~A" (homeserver session) (file-namestring file))
                                  :headers `(("Authorization" . ,(format nil "Bearer ~A" (token session)))
                                             ("Content-Type" . "application/json"))
                                  :content file
                                  :want-stream t))))

(defun get-direct-chats (session &key (want-stream nil))
  "Retrieves the list of direct chats from the user's account data.
   Arguments:
   SESSION: an instance of MATRIX-SESSION
   :WANT-STREAM: Boolean that determines whether a hash table or stream is returned."
  (matrix-get session (format nil "user/~A/account_data/m.direct" (mxid session)) :want-stream want-stream))

(defun create-room (session create-room-args-obj &key (want-stream nil))
  "Creates a new room using the slot values from CREATE-ROOM-OBJ. Returns a hash table or stream containing the homeserver
   response.
   Arguments:
   SESSION: an instance of MATRIX-SESSION
   CREATE-ROOM-OBJ: an instance of CREATE-ROOM
   :WANT-STREAM: a boolean that determines whether a stream or hash table will be returned"
  (matrix-post session "createRoom" :content-object create-room-args-obj :want-stream want-stream))

(defun list-rooms (homeserver &key (want-stream nil))
  "Retrieves the list of public rooms from the homeserver listed in SESSION. Returns either a hash table or stream.
   Arguments:
   HOMESERVER: URL of homeserver for which the room listing is requested without protocol prefix (eg \"matrix.server.tld\")
   :WANT-STREAM: a boolean which determines whether a stream or hash table is returned"
  (let ((response (dex:get (format nil "https://~A/_matrix/client/r0/publicRooms" homeserver)
                           :want-stream t)))
    (if want-stream response (yason:parse response))))
