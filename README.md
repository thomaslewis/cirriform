# cirriform
### _Thomas Lewis <thomaslewis1096@tutanota.com>_

A stateless [Matrix](https://matrix.org) client library in Common Lisp.  
Clone the git repo in your ASDF path and load with `(asdf:load-system "cirriform")`. If using for a client application, include  
#:cirriform in the :depends-on section of your ASDF system definition.

## Dependencies
_Dependencies are available from Quicklisp_

yason  
cl-json  
dexador  

## Exported Functions

* password-login  
* get-access-token  
* joined-rooms  
* send-room-message  
* sync  
* logout  
* purge  
* purge-status-query  
* deactivate  
* ban  
* kick  
* get-room-id  
* remove-alias  
* add-alias  
* get-room-tags  
* set-room-tag  
* reset-password  
* send-state-object  
* get-room-state-object  
* redact  
* get-pushers  
* pushers-set  
* get-push-rules  
* get-single-push-rule  
* delete-push-rule  
* add-push-rule  
* push-rule-enabled?  
* set-push-rule-enabled  
* get-push-rule-actions  
* change-push-rule-actions  
* invite-to-room  
* join-room  
* leave-room  
* forget-room  
* unban-user  
* upload-filter  
* get-filter  
* get-room-messages  
* register-account  
* media-post  

## Exported Classes (and their slot accessors)
* **matrix-session**  
  - mxid  
  - token  
  - homeserver  
  - identity-server  
  - txn-id  
  - device-id  
  - display-name  
  - rooms  
  - next-batch  
* **filter-event-filter**  
  - limit  
  - not-senders  
  - not-types  
  - senders  
  - types  
* **filter-room-filter**  
  - \[inherits from **filter-event-filter**\]  
  - not-rooms  
  - rooms  
  - contains-url  
* **filter-room**  
  - not-rooms  
  - rooms  
  - ephemeral  
  - include-leave  
  - state  
  - timeline  
  - account-data  
* **filter**  
  - event-fields  
  - event-format  
  - presence  
  - account-data  
  - room-slot  
* **create-invite-3pid**  
  - id-server  
  - medium  
  - address  
* **create-state-event**  
  - type-slot  
  - state-key  
  - content  
* **create-room-args**  
  - visibility  
  - room-alias  
  - room-name  
  - topic  
  - invite  
  - creation-content  
  - preset  
  - is-direct  
  - power-level  
* **create-room-with-3pid-invite**  
  - \[inherits from **create-room-args**\]  
  - invite-3pid  
* **create-room-with-initial-state**  
  - \[inherits from **create-room-args**\]  
  - initial-state  
* **pusher-data**  
  - url  
  - pusher-data-format  
* **pusher**  
  - pushkey  
  - kind  
  - app-id  
  - app-display-name  
  - device-display-name  
  - profile-tag  
  - lang  
  - data  
  - append-pusher  
* **push-condition**  
  - push-kind  
  - event-key  
  - event-pattern  
  - count-is  
* **push-rule**  
  - actions  
  - push-conditions  
  - content-pattern  
* **power-levels**  
  - ban-pl  
  - events-pl  
  - events-default-pl  
  - invite-pl  
  - kick-pl  
  - redact-pl  
  - state-default-pl  
  - users-pl  
  - users-default-pl  
  - notifications-pl  
* **room-state**  
  - join-rules  
  - power-levels  
  - room-name  
  - room-topic  
  - room-avatar  
  - pinned-events  
  - room-member-list  
* **matrix-room**  
  - room-id  
  - aliases  
  - room-state  
  - tags  
* **new-user**  
  - auth  
  - bind-email  
  - username  
  - password  
  - dev-id  
  - init-dev-display-name  
  - inhibit-login  

## License

BSD 2-clause "simplified" license

