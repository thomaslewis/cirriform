;;;(in-package :cirriform)

(in-package :cirriform)

(defclass matrix-session ()
  ((mxid :initarg :mxid
         :initform nil
         :accessor mxid)
   (token :initarg :token
          :initform nil
          :accessor token)
   (homeserver :initarg :homeserver
               :initform nil
               :accessor homeserver)
   (identity-server :initarg :identity-server
                    :accessor identity-server)
   (txn-id :initform (random 100000)
           :accessor txn-id)
   (device-id :initarg :device-id
              :accessor device-id)
   (display-name :initarg :display-name
                 :accessor display-name)
   (rooms :initarg :rooms
          :initform (make-hash-table)
          :accessor rooms)
   (next-batch :initarg :next-batch
               :accessor next-batch)))

(defclass filter-event-filter ()
  ((limit :initarg :limit
          :initform 50
          :accessor limit)
   (not_senders :initarg :not-senders
                :accessor not-senders)
   (not_types :initarg :not-types
              :accessor not-types)
   (senders :initarg :senders
            :accessor senders)
   (types :initarg :types
          :accessor types)))

(defclass filter-room-filter (filter-event-filter)
  ((not_rooms :initarg :not-rooms
              :accessor not-rooms)
   (rooms :initarg :rooms
          :accessor rooms)
   (contains_url :initarg :contains-url
                 :accessor contains-url)))

(defclass filter-room ()
  ((not_rooms :initarg :not-rooms
              :accessor not-rooms)
   (rooms :initarg :rooms
          :accessor rooms)
   (ephemeral :initarg :ephemeral
              :accessor ephemeral)
   (include_leave :initarg :include-leave
                  :accessor include-leave)
   (state :initarg :state
          :accessor state)
   (timeline :initarg :timeline
             :accessor timeline)
   (account_data :initarg :account-data
                 :accessor account-data)))

(defclass filter ()
  ((event_fields :initarg :event-fields
                 :accessor event-fields)
   (event_format :initarg :event-format
                 :initform "client"
                 :accessor event-format)
   (presence :initarg :presence
             :accessor presence)
   (account_data :initarg :account-data
                 :accessor account-data)
   (room :initarg :room
         :accessor room-slot)))

(defclass create-invite-3pid ()
  ((id_server :initarg :id-server
              :accessor id-server)
   (medium :initarg :medium
           :initform "email"
           :accessor medium)
   (address :initarg :address
            :accessor address)))

(defclass create-state-event ()
  ((type :initarg :type
         :accessor type-slot)
   (state_key :initarg :state-key
              :initform ""
              :accessor state-key)
   (content :initarg :content
            :initform "{}"
            :accessor content)))

(defclass create-room-args ()
  ((visibility :initarg :visibility
               :accessor visibility)
   (room_alias_name :initarg :room-alias
                    :accessor room-alias)
   (name :initarg :room-name
         :accessor room-name)
   (topic :initarg :topic
          :accessor topic)
   (invite :initarg :invite
           :accessor invite)
   (creation_content :initarg :creation-content
                     :accessor creation-content)
   (preset :initarg :preset
           :accessor preset)
   (is_direct :initarg :is-direct
              :accessor is-direct)
   (power_level_content_override :initarg :power-level
                                 :accessor power-level-override)))

(defclass create-room-with-3pid-invite (create-room-args)
  ((invite_3pid :initarg :invite-3pid
                :initform (make-instance 'create-invite-3pid)
                :accessor invite-3pid)))

(defclass create-room-with-initial-state (create-room-args)
  ((initial_state :initarg :initial-state
                  :accessor initial-state)))

(defclass pusher-data ()
  ((url :initarg :url
        :accessor url)
   (format :initarg :pusher-data-format
           :initform "event_id_only"
           :accessor pusher-data-format)))

(defclass pusher ()
  ((pushkey :initarg :pushkey
            :accessor pushkey)
   (kind :initarg :pushkey
         :initform "http"
         :accessor kind)
   (app_id :initarg :app-id
           :accessor app-id)
   (app_display_name :initarg :app-display-name
                     :accessor app-display-name)
   (device_display_name :initarg :device-display-name
                        :accessor device-display-name)
   (profile_tag :initarg :profile-tag
                :accessor profile-tag)
   (lang :initarg :lang
         :initform "en-US"
         :accessor lang)
   (data :initarg :data
         :initform (make-instance 'pusher-data)
         :accessor data)
   (append :initarg :append-pusher
           :initform "false"
           :accessor append-pusher)))

(defclass push-condition ()
  ((kind :initarg :push-kind
         :initform (list "event_match")
         :accessor push-kind)
   (key :initarg :event-key
        :accessor event-key)
   (pattern :initarg :event-pattern
            :accessor event-pattern)
   (is :initarg :count-is
       :accessor count-is)))

(defclass push-rule ()
  ((actions :initarg :actions
            :initform (list "notify")
            :accessor actions)
   (conditions :initarg :push-conditions
               :initform (list (make-instance 'push-condition))
               :accessor push-conditions)
   (pattern :initarg :content-pattern
            :accessor content-pattern)))

(defclass power-levels ()
  ((ban :initarg :ban
        :initform 50
        :accessor ban-pl)
   (events :initarg :events
           :accessor events-pl)
   (events_default :initarg :events-default
                   :initform 0
                   :accessor events-default-pl)
   (invite :initarg :invite
           :initform 50
           :accessor invite-pl)
   (kick :initarg :kick
         :initform 50
         :accessor kick-pl)
   (redact :initarg :redact
           :initform 50
           :accessor redact-pl)
   (state_default :initarg :state-default
                  :initform 50
                  :accessor state-default-pl)
   (users :initarg :users
          :accessor users-pl)
   (users_default :initarg :users-default
                  :initform 0
                  :accessor users-default-pl)
   (notifications :initarg :notifications
                  :initform '(("room" . 50))
                  :accessor notifications-pl)))

(defclass room-state ()
  ((join_rules :initarg :join-rules
               :initform "public"
               :accessor join-rules)
   (power_levels :initarg :power-levels
                 :accessor power-levels)
   (name :initarg :name
         :accessor room-name)
   (topic :initarg :topic
          :accessor room-topic)
   (avatar :initarg :avatar
           :accessor room-avatar)
   (pinned_events :initarg :pinned-events
                  :accessor pinned-events)
   (member_list :initarg :member-list
                :accessor room-member-list)))

(defclass matrix-room ()
  ((room-id :initarg :room-id
            :accessor room-id)
   (aliases :initarg :aliases
            :accessor aliases)
   (room-state :initarg :room-state
               :initform (make-instance 'room-state)
               :accessor room-state)
   (tags :initarg :tags
         :accessor tags)
   (room-messages :accessor room-messages)
   (next-msgs-token :accessor next-token)))

(defclass new-user ()
  ((auth :initarg :auth
         :accessor auth)
   (bind_email :initarg :bind-email
               :accessor bind-email)
   (username :initarg :username
             :accessor username)
   (password :initarg :password
             :accessor password)
   (device_id :initarg :device-id
              :accessor dev-id)
   (inital_device_display_name :initarg :init-dev-display-name
                               :accessor init-dev-display-name)
   (inhibit_login :initarg :inhibit-login
                  :accessor inhibit-login)))

(defclass event-base ()
  ((event-content :initarg :event-content
                  :accessor event-content)
   (event-type :initarg :event-type
               :accessor event-type)
   (sender :initarg :sender
           :accessor sender)
   (origin-server-ts :initarg :origin-hs-ts
                     :accessor origin-hs-ts)
   (unsigned-event-data :initarg :unsigned-event-data
                        :accessor unsigned-event-data
                        :initform (make-instance 'unsigned-data))
   (event-room-id :initarg :event-room-id
                  :accessor event-room-id)))

(defclass unsigned-data ()
  ((age :initarg :age
        :accessor unsigned-age)
   (invite-room-state :initarg :invite-room-state
                      :accessor invite-room-state)
   (redacted-because :initarg :redacted-because
                     :accessor redacted-because))
